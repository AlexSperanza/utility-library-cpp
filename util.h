#ifndef UTIL_H_INCLUDED
#define UTIL_H_INCLUDED

#include <vector>

class util{
public:
	
	//linear proportion
	static void lin_prop(double* value, double var, double low_bound, double up_bound, double min_value, double max_value);
	
	//normalize any double vector; the greater value is 1, other are between 1 and 0
	static void normalize(std::vector<double>* container);
	
	//modular power
	static long long int pow_mod(long long int base, long long int exp, long long int n);

	//change state of  boolean variable
	static void changeState(bool* variable);
};

#endif // UTIL_H
