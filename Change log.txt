{ Objectives for next version: }
-- change all return functions to pointer function (they would not return anything but they would change the variable itself).
[Version 1.1]
- Changed the funtions "lin_prop" and "normalize" so they change the variables instead of returning the corresponding object.
- Updated instructions about previous changes.


[Version 1.0]

- Added to GitHub.
- Added 4 new functions for linear proportions (lin_prop), vector (of double) normalizing (normalize), power for big numbers (pow_mod), changing the state of a boolean variable(changeState).
