#include "util.h"
#include <iostream>
#include <vector>

void util::lin_prop(double* value, double var, double low_bound, double up_bound, double min_value, double max_value){
	*value = ((var - min_value)*(up_bound - low_bound)/(max_value - min_value)) + low_bound;
}

void util::normalize(std::vector<double>* container){
	double record = -0.0;
	
	for (unsigned int i = 0; i < container->size(); i++){
		if (record < container->at(i)){
			record = container->at(i);
		}
	}

	for (unsigned int i = 0; i < container->size(); i++){
		util::lin_prop(&container->at(i), container->at(i), 0, 1, 0, record);
		//the function uses the address of the value to change the value itself
	}
}

long long int util::pow_mod(long long int base, long long int exp, long long int n){
    long long int res;
    res = base;
    for(long long int i = 0; i < exp - 1; i++){
        res = ((res % n) * (base % n)) % n;
    }
    return res;
}

void util::changeState(bool* variable){
	if(*variable){	//variable = 1, so change it to 0 (true to false)
		*variable = 0;
	}
	else{			//variable = 0, so change it to 1 (false to true)
		*variable = 1;
	}
}
